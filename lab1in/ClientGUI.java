package lab1in;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

public class ClientGUI extends JFrame
{
    private JLabel status;
    private JButton connect;
    private JButton submit;
    private JButton stop;
    
    private class EventHandler implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();  
            System.out.println(command + " Button Pressed");
          }		
    }
    public ClientGUI(String title)
    {
            int i = 0;
            this.setTitle(title);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
        
            //ADD YOUR CODE HERE TO CREATE THE STATUS JLABEL AND THE JBUTTONS
            // North Panel
            JPanel north = new JPanel(new FlowLayout());     
            this.getContentPane().add(north, BorderLayout.NORTH);
            
            // Red connection text
            JLabel connLabel = new JLabel("Not Connected");
            connLabel.setForeground(Color.red);
            north.add(connLabel);
            
            //South Panel
            JPanel south = new JPanel(new FlowLayout());
            this.getContentPane().add(south, BorderLayout.SOUTH);
            
            // 3 Buttons
            JButton connect = new JButton("Connect");
            JButton submit  = new JButton("Submit");
            JButton stop    = new JButton("Stop");
            // Event Handling 
            connect.addActionListener(new EventHandler ()); 
            submit.addActionListener (new EventHandler ()); 
            stop.addActionListener   (new EventHandler ()); 
            south.add(connect);
            south.add(submit);
            south.add(stop);
         
            this.pack();
            this.setVisible(true);
    }
    
    public static void main(String[] args)
    {
        
        try {
            // Set cross-platform Java L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException e){}
        catch (ClassNotFoundException e) {}
        catch (InstantiationException e) {}
        catch (IllegalAccessException e) {}
        
        new ClientGUI(args[0]); //args[0] represents the title of the GUI
    }
}